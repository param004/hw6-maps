﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using Xamarin.Forms;
using Xamarin.Forms.Maps;
using Xamarin.Forms.Xaml;

namespace CS481MapsHomeWork
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CS481MapsHomeWorkPage : ContentPage
    {
        public CS481MapsHomeWorkPage()
        {
            InitializeComponent();

            var initialMapLocation = MapSpan.FromCenterAndRadius
                                            (new Position(33.1307785, -117.1601826)
                                             , Distance.FromMiles(100));

            SuperMap.MoveToRegion(initialMapLocation);


            PlaceAMarker();
            Picker();
        }

        void Handle_ClickedHybrid(object sender, System.EventArgs e)
        {
            SuperMap.MapType = MapType.Hybrid;
        }

        void Handle_ClickedSatellight(object sender, System.EventArgs e)
        {
            SuperMap.MapType = MapType.Satellite;
        }

        void Handle_ClickedStreet(object sender, System.EventArgs e)
        {
            SuperMap.MapType = MapType.Street;
        }

        void Handle_IndexChanged(object sender, System.EventArgs e)
        {
            var selector = (Picker)sender;
            int touchIndex = selector.SelectedIndex;

            switch(touchIndex)
            {
                case (0):
                    SuperMap.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(33.1307785, -117.1601826), Distance.FromMiles(1)));
                    break;
                case (1):
                    SuperMap.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(32.99999999, -117.1601826), Distance.FromMiles(10)));
                    break;
                case (2):
                    SuperMap.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(52, 0), Distance.FromMiles(100)));
                    break;
                case (3):
                    SuperMap.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(13.0500, 80.2824), Distance.FromMiles(15)));
                    break;
                case (4):
                    SuperMap.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(51.47, -0.4543), Distance.FromMiles(1)));
                    break;
                case (5):
                    SuperMap.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(25, -71), Distance.FromMiles(50)));
                    break;

            }
        }


        private void Picker()
        {
            PointsSelectors.SelectedIndex = 0;

            var PointsSelectorSource = new ObservableCollection<string>();

            PointsSelectorSource.Add("CSUSM");
            PointsSelectorSource.Add("Black Mountain Ranch");
            PointsSelectorSource.Add("London");
            PointsSelectorSource.Add("Chennai, India");
            PointsSelectorSource.Add("London Heathrow");          
            PointsSelectorSource.Add("Bermuda Triangle");

            PointsSelectors.ItemsSource = PointsSelectorSource;

            
        }


        private void PlaceAMarker()
        {
            var position = new Position(33.1307785, -117.1601826); // Latitude, Longitude
            var pin = new Pin
            {
                Type = PinType.Place,
                Position = position,
                Label = "CSUSM",
                Address = "The University for Cougars Mascot"
            };

            var position2 = new Position(32.99999999, -117.1601826);
            var pin2 = new Pin
            {
                Type = PinType.Place,
                Position = position2,
                Label = "Black Mountain Ranch",
                Address = "Wow! Black Mountains."    
            };

            var position3 = new Position(52, 0);
            var pin3 = new Pin
            {
                Type = PinType.Place,
                Position = position3,
                Label = "London",
                Address = "The World Famous City"
            };

            var position4 = new Position(13.0500, 80.2824);
            var pin4 = new Pin
            {
                Type = PinType.Place,
                Position = position4,
                Label = "Chennai, India",
                Address = "The Marina Beach. One of the largest beach in world"
            };

            var position5 = new Position(51.47, -0.4543);
            var pin5 = new Pin
            {
                Type = PinType.Place,
                Position = position5,
                Label = "London Heathrow",
                Address = "The worlds busiest airport"
            };

            var position6 = new Position(25,-71);
            var pin6 = new Pin
            {
                Type = PinType.Place,
                Position = position6,
                Label = "Bermuda Triangle",
                Address = "A dangerous place, No fly zone"
            };


            SuperMap.Pins.Add(pin);
            SuperMap.Pins.Add(pin2);
            SuperMap.Pins.Add(pin3);
            SuperMap.Pins.Add(pin4);
            SuperMap.Pins.Add(pin5);
            SuperMap.Pins.Add(pin6);
        }
    }
}